import java.util.HashMap;

public class Solution {
  //  private HashMap<Character, Integer> map;

    public static void main(String[] args) {
        Solution solution = new Solution();
  //      solution.initialize();
        System.out.println(solution.romanToInt("LVIII"));
        System.out.println(solution.romanToInt("IV"));
        System.out.println(solution.romanToInt("MCMXCIV"));
        System.out.println(solution.romanToInt("III"));
    }

    private int romanToInt(String s){

        int result =0;
        int last = 0;

        for (int i = s.length()-1; i>=0; i--) {
//            int tmp = map.get(s.charAt(i));
            int tmp = charToInt(s.charAt(i));
            if (tmp<last)
                result -= tmp;
            else
                result += tmp;
            last = tmp;
        }

        return result;
    }

    private int charToInt(char c) {
        switch (c){
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                break;
        }
        return 0;
    }

    /*private void initialize() {
        map = new HashMap<>();
        map.put('I',1);
        map.put('V',5);
        map.put('X',10);
        map.put('L',50);
        map.put('C',100);
        map.put('D',500);
        map.put('M',1000);
    }*/
}
